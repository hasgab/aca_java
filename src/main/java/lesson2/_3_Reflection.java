package lesson2;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@interface Metadata {
   String author();

   String version();
}

@Metadata(
   author = "Vachagan",
   version = "0.1"
)
class AnnotatedType<T> {

   @Metadata(
      author = "Vachagan",
      version = "0.1"
   )
   T m1(T arg) {
      return arg;
   }
}

public class _3_Reflection {

   public static void main(String[] args) {
      AnnotatedType instance = new AnnotatedType();


      Metadata declaredAnnotation = instance.getClass().getDeclaredAnnotation(Metadata.class);
      System.out.println(declaredAnnotation.author());
      System.out.println(declaredAnnotation.version());

      for (Method declaredMethod : instance.getClass().getDeclaredMethods()) {
         System.out.println(declaredMethod.getName());
         System.out.println("\t args");
         for (Class<?> parameterType : declaredMethod.getParameterTypes()) {
            System.out.println("\t\t " + parameterType.getName());
         }
         System.out.println("\t returns");
         System.out.println("\t\t " + declaredMethod.getReturnType().getName());
      }



   }
}
