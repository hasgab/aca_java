package lesson2;


public class _7_Searching {

   // binary search O(log N) logarithmic complexity
   static int search(int a[], int numberToFind) {
      int low = 0;
      int high = a.length - 1;
      int mid;
      while (low <= high) {
         mid = low + ((high - low) / 2);
         if (a[mid] == numberToFind) {
            return mid;
         } else if (a[mid] < numberToFind) {
            low = mid + 1;
         } else {
            high = mid - 1;
         }
      }
      return -1;
   }

   public static void main(String args[]) {
      final int a[] = {1, 2, 4, 5, 7, 8};

      System.out.println(search(a, -1));
      System.out.println(search(a, 1));
      System.out.println(search(a, 8));
      System.out.println(search(a, 2));
   }

}
