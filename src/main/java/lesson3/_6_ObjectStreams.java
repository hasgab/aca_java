package lesson3;

import java.io.*;
import java.util.Arrays;

import static lesson3.Shared.log;

public class _6_ObjectStreams {

   static class User implements Serializable {
      final String firstName;
      final String lastName;
      final int age;

      User(String firstName, String lastName, int age) {
         this.firstName = firstName;
         this.lastName = lastName;
         this.age = age;
      }

      @Override
      public String toString() {
         return "User{" +
            "firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", age=" + age +
            '}';
      }
   }


   public static void main(String[] args) throws IOException {
      byte[] bytes = null;
      User user = new User("Jon", "Doe", 42);

      try (
         ByteArrayOutputStream bos = new ByteArrayOutputStream();
         ObjectOutputStream out = new ObjectOutputStream(bos)
      ) {
         log("writing : " + user);
         out.writeObject(user);
         out.flush();
         bytes = bos.toByteArray();
      }

      System.out.println(bytes.length + " bytes : " + Arrays.toString(bytes));

      try (
         ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
         ObjectInputStream in = new ObjectInputStream(bis)
      ) {
         log("reading : " + in.readObject());
      } catch (ClassNotFoundException e) {
         e.printStackTrace();
      }
   }


}
